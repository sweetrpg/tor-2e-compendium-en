# tor-2e-compendium-en
TOR2E - Unofficial compendium for The One Ring 2nd edition - english version


## Installation
Install from FoundryVTT setup / Add-on modules
- Search TOR2E in order to find the module
or
- Install module with Manifest URl : https://gitlab.com/ghorin/tor-2e-compendium-en/-/raw/main/module.json


## Description
This module install a compendium that contains mny items useful for creating characters (PCs, NPCs, Adversaries) in The One Ring 2e system (https://foundryvtt.com/packages/tor2e) :
- creation of Distinctive Features, Virtues, Flaws, Fell abilities, skills and Rewards
- creation of weapons, armour and shields
- creation of adversaries from 
    . 2nd edition
    . 1st edition : 2 versions of adversaries conversion from 1e to 2e
        - by CircleOfNom
        - by Ghorin

Special thanks to CircleOfNom who did a great work on his adversaries converion and for accepting that I share his work in this compendium.

All those items are created 
- with their stats 
- without any text or image from the official books
- with a reference where to find the text and images in the books

All the images used for the items are images provided 
- either by TOR2E system => Distinctive Features, Virtues, Flaws, Fell abilities and Rewards
- or by Foundry VTT (in Foundry VTT installation folder, inside resources/app/public/icons subfolder) => weapons, armours, shields 

Note : There is no Active Effect implemented in any item of this compendium.

## License
The One Ring, Middle-­earth, and The Lord of the Rings and the characters, items, events, and places therein are trademarks or registered trademarks of the Saul Zaentz Company d/b/a Middle-­earth Enterprises (SZC) and are used under license by Sophisticated Games Ltd. and their respective licensees. All rights reserved.

## Ready-to-use / Ready-to-modify
As per property right and license, and as this compendium module is public : it contains no text description, no rule text, no image from any The One Ring RPG books. But all the items, after being created in a Loremaster Foundry VTT game, are in a privacy mode / not shared to public, and then the Loremaster may modify them to add any needed description and image from his own core book copy.


## How it works
- Go into the "Compendium" tab. You will find following packs :
    . tor-2e-compendium-en : Equipment
    . tor-2e-compendium-en : Characteristics
    . tor-2e-compendium-en : Adversaries
- You may either 
    . open a pack and drag&drop contents to the right tab (Objects for Characteristics and Equipment, Actors for Adversaries)
    . or do a right clic droit on the packs and import all contents into your world
Foundry VTT documentation about Compendiums :  https://foundryvtt.com/article/compendium/
